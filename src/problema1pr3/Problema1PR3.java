package problema1pr3;

public class Problema1PR3 {

    public static void main(String[] args) {
        System.out.println("Problema 1: " + problema1(1, 1));
        System.out.println("Problema 2: " + problema2(9));
        System.out.println("Problema 3: " + problema3("Esto funciona y no se como"));
        int[] v = {1, 4, 2, 0, 5};
        System.out.println("Problema 4: " + problema4(v, 2));
        System.out.println("Problema 5: " + problema5(4, 8));
        System.out.println("Problema 6: " + problema6(8));
        System.out.println("Problema 7: " + problema7(v));
        System.out.println("Problema 9: " + problema9(4, 8));
        System.out.println("Problema 10: " + problema10(9));
        int[] v1 = {1, 2};
        System.out.println("Problema 11: " + problema11(v1));
        System.out.println("Problema 12: " + problema12(v1));
        int[] v2 = {5, 2, 9, 6, 3, 20, 5, 7, 7, 6, 8, 2, 7, 5, 0, 2, 5, 2, 5, 41, 56, 0, 52, 52, 52, 52, 524, 6, 52, -9, 12, 4};
        System.out.println("Problema 13: " + problema13(v2));
        System.out.println("Problema 14: " + problema14("patliiiio", "iatho"));
    }

    public static int problema1(int n, int m) {
        int[][] f = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            f[i][0] = i;
        }

        for (int j = 0; j <= m; j++) {
            f[0][j] = j;
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                int value = Math.max(f[i - 1][j] + j, 2 * f[i - 1][j - 1]);
                value = Math.max(value, f[i][j - 1] + i);
                f[i][j] = value;
            }
        }

        return f[n][m];
    }

    public static int problema2(int n) {
        int[] f = new int[n + 1];

        int min = (3 > f.length ? f.length : 3);

        for (int i = 0; i < min; i++) {
            f[i] = i;
        }

        for (int i = 3; i <= n; i++) {
            f[i] = f[i - 1] + f[i - 2] / 2;
        }

        return f[n];

    }

    public static int problema3(String s) {
        char[] c = s.toCharArray();

        return auxLongitudPalabraMaxima(c, 0, c.length - 1);
    }

    public static int auxLongitudPalabraMaxima(char[] v, int ini, int fin) {
        if ((fin - ini) == 0) {
            return ((v[ini] == ' ') ? 0 : 1);
        }
        int med = (ini + fin) / 2;

        int maxPalabraIzq = auxLongitudPalabraMaxima(v, ini, med);
        int maxPalabraDer = auxLongitudPalabraMaxima(v, med + 1, fin);

        int centroIzq = 0;
        for (int i = med; i >= ini; i--) {
            if (v[i] != ' ') {
                centroIzq++;
            } else {
                break;
            }
        }

        int centroDer = 0;
        for (int i = med + 1; i <= fin; i++) {
            if (v[i] != ' ') {
                centroDer++;
            } else {
                break;
            }
        }

        int value = Math.max(maxPalabraDer, maxPalabraIzq);
        return Math.max(value, centroDer + centroIzq);

    }

    public static int problema5(int n, int m) {
        int[][] f = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            f[i][0] = i;
        }

        for (int j = 0; j <= m; j++) {
            f[0][j] = j;
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                int value = Math.max(f[i - 1][j] + j * j, f[i][j] + i);
                f[i][j] = value;
            }
        }

        return f[n][m];
    }

    public static int problema6(int n) {
        int[] f = new int[n + 1];

        int min = (3 > f.length ? f.length : 3);

        for (int i = 0; i < min; i++) {
            f[i] = i;
        }

        for (int i = 3; i <= n; i++) {
            f[i] = (f[i - 1] + 3 * f[i - 2]) / 2;
        }

        return f[n];

    }

    public static int problema7(int[] v) {
        return auxProblema7(v, 0, v.length - 1);
    }

    private static int auxProblema7(int[] v, int ini, int fin) {
        if (fin - ini == 0) {
            if (v[ini] == 0) {
                return 1;
            } else {
                return 0;
            }
        }
        int med = (ini + fin) / 2;
        int maximaIzq = auxProblema7(v, ini, med);
        int maximaDer = auxProblema7(v, med + 1, fin);

        int centroIzq = 0;
        for (int i = med; i >= ini; i--) {
            if (v[i] == 0) {
                centroIzq++;
            } else {
                break;
            }
        }

        int centroDer = 0;
        for (int i = med + 1; i <= fin; i++) {
            if (v[i] == 0) {
                centroDer++;
            } else {
                break;
            }
        }

        int value = Math.max(maximaIzq, maximaDer);
        return Math.max(value, centroDer + centroIzq);
    }

    public static int problema9(int n, int m) {
        int[][] f = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            f[i][0] = 0;
        }

        for (int j = 0; j <= m; j++) {
            f[0][j] = 0;
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (i == j) {
                    f[i][j] = f[i - 1][j - 1] + 1;
                } else {
                    f[i][j] = Math.max(f[i - 1][j], f[i][j - 1]) + 2;
                }
            }
        }

        return f[n][m];
    }

    public static int problema10(int n) {
        int[] f = new int[n + 1];

        int min = (3 > f.length ? f.length : 3);

        for (int i = 0; i < min; i++) {
            f[i] = i;
        }

        for (int i = 3; i <= n; i++) {
            f[i] = f[i - 1] / 2 + f[i - 2] / 2;
        }

        return f[n];
    }

    public static int problema11(int[] v) {
        return auxProblema11(v, 0, v.length - 1);
    }

    private static int auxProblema11(int[] v, int ini, int fin) {
        if (fin - ini < 1) {
            if (fin - ini == 0) {
                return v[ini];
            } else {
                return 1;
            }
        }
        int med = (ini + fin) / 2;
        int maxProdIzq = auxProblema11(v, ini, med);
        int maxProdDer = auxProblema11(v, med + 1, fin);

        int maximaCentroIzquierda = 1;
        int centroIzquierda = 1;
        for (int i = med; i >= ini; i--) {
            centroIzquierda *= v[i];
            if (centroIzquierda > maximaCentroIzquierda) {
                maximaCentroIzquierda = centroIzquierda;
            }
        }

        int maximaCentroDerecha = 1;
        int centroDerecha = 1;
        for (int i = med + 1; i <= fin; i++) {
            centroDerecha *= v[i];
            if (centroDerecha > maximaCentroDerecha) {
                maximaCentroDerecha = centroDerecha;
            }
        }

        int value = Math.max(maxProdDer, maxProdIzq);
        return Math.max(value, maximaCentroDerecha * maximaCentroIzquierda);

    }

    public static int problema12(int[] v) {
        return auxProblema12(v, 0, v.length - 1) + 1;
    }

    public static int auxProblema12(int[] v, int ini, int fin) {
        if (ini == fin) {
            if (v[ini] == ini) {
                return ini;
            }
            return 0;
        }
        int med = (ini + fin) / 2;
        return Math.max(auxProblema12(v, ini, med), auxProblema12(v, med + 1, fin));
    }

    public static int problema13(int[] v) {
        return auxProblema13(v, 0, v.length - 1);
    }

    public static int auxProblema13(int[] v, int ini, int fin) {
        if (ini == fin) {
            return v[ini];
        }
        int med = (ini + fin) / 2;
        return Math.min(auxProblema13(v, ini, med), auxProblema13(v, med + 1, fin));
    }

    public static int problema14(String x1, String y1) {
        char[] x = x1.toCharArray();
        char[] y = y1.toCharArray();
        return auxProblema14(x, y);
    }

    private static int auxProblema14(char[] x, char[] y) {
        int[][] f = new int[x.length + 1][y.length + 1];

        for (int i = 0; i <= x.length; i++) {
            f[i][0] = 0;
        }

        for (int j = 0; j <= y.length; j++) {
            f[0][j] = 0;
        }

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                if (x[i] == y[j]) {
                    f[i + 1][j + 1] = f[i][j] + 1;
                } else {
                    f[i + 1][j + 1] = Math.max(f[i + 1][j], f[i][j + 1]);
                }

            }
        }
        return f[x.length][y.length];
    }
    
    public static int problema4(int[]v, int pos){
        return division(v,0,v.length-1,pos);
    }

    public static int division(int[] vec, int linf, int lsup, int piv) {
        int pini = linf;
        int pfin = lsup;

        while (pini <= pfin) {
            while ((pini <= lsup) && vec[pini] <= vec[piv]) {
                pini++;
            }
            while ((pfin >= linf) && vec[pini] >= vec[piv]) {
                pfin--;
            }

            if (pini < pfin) {
                int aux = vec[pini];
                vec[pini] = vec[pfin];
                vec[pfin] = aux;
                pini++;
                pfin--;
            }
        }

        if (pini < piv) {
            int aux = vec[pini];
            vec[pini] = vec[piv];
            vec[piv] = aux;
            pini++;
        } else if (pfin > piv) {
            int aux = vec[piv];
            vec[piv] = vec[pfin];
            vec[pfin] = aux;
            pfin--;
        }
        
        return vec[piv];
    }
}
